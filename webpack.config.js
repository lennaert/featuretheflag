const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");
const merge = require("webpack-merge");

const baseConfig = {
    entry: {
        "popup/index": "./src/popup/index.tsx",
        "config/index": "./src/config/index.tsx"
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].js",
        libraryTarget: "umd",
        library: "FeatureFlags",
        umdNamedDefine: true
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    devtool: "source-map",
    optimization: {
        // filled in in env-specific config
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/
            },
            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: "src/popup",
                to: "popup",
                ignore: ["*.ts", "*.tsx"]
            },
            {
                from: "src/config",
                to: "config",
                ignore: ["*.ts", "*.tsx"]
            },
            {
                from: "src/manifest.json",
                to: "./"
            },
            {
                from: "src/icon128.png",
                to: "./"
            },
            {
                from: "src/icon48.png",
                to: "./"
            }
        ])
    ]
};

module.exports = env => {
    return merge(baseConfig, require(`./webpack.config.${env.NODE_ENV}.js`));
};
