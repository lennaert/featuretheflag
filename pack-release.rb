#!/usr/bin/env ruby

def main
  yarn

  new_tag = get_new_tag_from_user

  create_zip(new_tag)

  `git tag #{new_tag}`
end

def yarn
  `yarn clean`
  output = `yarn dist`

  if $? != 0
    puts "ERROR!"
    puts "output:"
    puts output

    exit 1
  end
end

def create_zip(tag)
  zip_filename = File.expand_path("dist-#{tag}.zip")

  system("zip", "-r", zip_filename, "dist")

  puts "Zip file created: #{zip_filename}"
end

def get_new_tag_from_user
  latest_version = determine_version

  $stdout.write "Create new tag name: "
  if latest_version
    $stdout.puts "Latest version was #{latest_version}"
  else
    $stdout.puts "It will be the first version"
  end

  loop do
    begin
      $stdout.write "> "

      input = gets

      new_version = Version.new(input.strip)

      if latest_version && new_version <= latest_version
        puts "The version should be larger than #{latest_version}"

        next

      end
      return new_version
    rescue ArgumentError => e
      puts "Error: #{e.message}"
    end
  end
end

def determine_version
  get_versions_from_git
    .map {|line| Version.new(line) }
    .sort
    .last
end

class Version
  include Comparable

  attr_reader :major, :minor, :teeny

  def initialize(input)
    puts "Building '#{input}'"
    @input = input

    parts = input.split(".")

    if parts.size != 3 || parts.any? {|part| part !~ /[0-9]+/ }
      raise ArgumentError, "Invalid version number #{input}. Must be of format 1.20.3"
    end

    @major, @minor, @teeny = parts
  end

  def <=>(other)
    if major != other.major
      return major <=> other.major
    end

    if minor != other.minor
      return minor <=> other.minor
    end

    teeny <=> other.teeny
  end

  def to_s
    @input
  end
end

def get_versions_from_git
  `git tag -l`.each_line.map(&:strip).reject(&:empty?)
end

main
