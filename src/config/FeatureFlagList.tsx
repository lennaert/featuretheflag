import * as React from "react";

import { observer } from "mobx-react";
import { DataTable, DataTableRow, DataTableCell, DataTableBody } from "@rmwc/data-table";
import { Icon } from "@rmwc/icon";
import { List, ListItemMeta, ListItemText, ListItem } from "@rmwc/list";

type Props = {
    domain: string;
    featureFlags: string[];
    onDelete: (domain: string, featureFlag: string) => void;
};

@observer
export class FeatureFlagList extends React.Component<Props> {
    render() {
        const { featureFlags } = this.props;

        return featureFlags.length > 0 ? (
            <List>{this.renderFeatureFlags(featureFlags)}</List>
        ) : (
            <div className="feature-flag-list no-feature-flags">No feature flags configured yet.</div>
        );
    }

    renderFeatureFlags(featureFlags: string[]) {
        return featureFlags.map((ff, i) => (
            <FeatureFlagEntry featureFlag={ff} onDelete={this.onDelete} key={ff} index={i} />
        ));
    }

    onDelete = (featureFlag: string) => {
        this.props.onDelete(this.props.domain, featureFlag);
    };
}

type EntryProps = {
    index: number;
    featureFlag: string;
    onDelete: (featureFlag: string) => void;
};

@observer
class FeatureFlagEntry extends React.Component<EntryProps> {
    render() {
        return (
            <ListItem>
                <ListItemText>{this.props.featureFlag}</ListItemText>
                <ListItemMeta icon="delete" onClick={this.onClickDelete} />
            </ListItem>
        );
    }

    onClickDelete = () => {
        this.props.onDelete(this.props.featureFlag);
    };
}
