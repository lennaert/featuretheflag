import { observer } from "mobx-react";

import * as React from "react";

import { FeatureFlagStore } from "../shared/FeatureFlagStore";
import { FeatureFlagList } from "./FeatureFlagList";

import { Card, CardActionIcon, CardActionIcons } from "@rmwc/card";

import { Typography } from "@rmwc/typography";
import { IconButton } from "@rmwc/icon-button";
import { Button } from "@rmwc/button";

type Props = {
    domain: string;
    store: FeatureFlagStore;
    onDeleteDomain: (domain: string) => void;
};

@observer
export class DomainConfiguration extends React.Component<Props> {
    render() {
        const { domain, store } = this.props;

        const featureFlags = store.featureFlagsForDomain(domain);

        return (
            <Card style={{ width: "400px", marginBottom: "20px" }}>
                <Typography
                    style={{ backgroundColor: "#d04c4c", padding: "10px" }}
                    use="headline6"
                    theme="textPrimaryOnDark">
                    {domain}
                </Typography>

                <FeatureFlagList domain={domain} featureFlags={featureFlags} onDelete={this.onDeleteFeatureFlag} />

                <Button onClick={this.onDeleteDomain} icon="delete">
                    Delete domain config
                </Button>
            </Card>
        );
    }

    onDeleteDomain = () => {
        this.props.onDeleteDomain(this.props.domain);
    };

    onDeleteFeatureFlag = (domain: string, featureFlag: string) => {
        this.props.store.delete(domain, featureFlag);
    };

    onAddFeatureFlag = (domain: string, featureFlag: string) => {
        this.props.store.add(domain, featureFlag);
    };
}
