import * as React from "react";
import * as ReactDOM from "react-dom";

import { ConfigPage } from "./ConfigPage";
import { FeatureFlagStore } from "../shared/FeatureFlagStore";

import "material-components-web/dist/material-components-web.min.css";

ReactDOM.render(<ConfigPage store={new FeatureFlagStore()} />, document.getElementById("root"));
