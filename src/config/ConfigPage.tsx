import { observer } from "mobx-react";
import * as React from "react";

import { FeatureFlagStore } from "../shared/FeatureFlagStore";
import { DomainConfigList } from "./DomainConfigList";
import { ThemeProvider } from "@rmwc/theme";
import { TopAppBar, TopAppBarRow, TopAppBarSection, TopAppBarTitle, TopAppBarFixedAdjust } from "@rmwc/top-app-bar";

type Props = {
    store: FeatureFlagStore;
};

@observer
export class ConfigPage extends React.Component<Props> {
    render() {
        return (
            <ThemeProvider
                options={{
                    primary: "#d04c4c",
                    secondary: "#5d5dff",
                    width: "100%"
                }}>
                <TopAppBar>
                    <TopAppBarRow>
                        <TopAppBarSection alignStart>
                            <TopAppBarTitle>Feature the Flag</TopAppBarTitle>
                        </TopAppBarSection>
                    </TopAppBarRow>
                </TopAppBar>
                <TopAppBarFixedAdjust />
                {this.props.store.loading ? <div>Loading ...</div> : <DomainConfigList store={this.props.store} />}
            </ThemeProvider>
        );
    }

    componentDidMount() {
        this.props.store.loadFromStorage();
    }
}
