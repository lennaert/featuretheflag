import * as React from "react";
import { FeatureFlagStore } from "../shared/FeatureFlagStore";
import { observer } from "mobx-react";
import { DomainConfiguration } from "./DomainConfiguration";

import { List } from "@rmwc/list";
import { Typography } from "@rmwc/typography";

type Props = {
    store: FeatureFlagStore;
};

@observer
export class DomainConfigList extends React.Component<Props> {
    render() {
        const { store } = this.props;

        return store.domains.length > 0 ? (
            <div
                style={{
                    width: "500px",
                    display: "flex",
                    justifyContent: "center"
                }}>
                <List>
                    {store.domains.map(domain => {
                        return (
                            <DomainConfiguration domain={domain} store={store} onDeleteDomain={this.onDeleteDomain} />
                        );
                    })}
                </List>
            </div>
        ) : (
            <div style={{ padding: "10px" }}>
                <Typography use="headline4">No domains configured yet</Typography>
                <br />
                <Typography use="subtitle1">
                    Use the extension icon when viewing a domain to add feature flags
                </Typography>
            </div>
        );
    }

    onDeleteDomain = (domain: string) => {
        this.props.store.deleteDomain(domain);
    };
}
