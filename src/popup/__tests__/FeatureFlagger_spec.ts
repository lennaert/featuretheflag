import { FeatureFlagger } from "../../popup/FeatureFlagger";

describe("FeatureFlagger", () => {
    it("adds a feature flag", () => {
        const subject = new FeatureFlagger("https://mendix.com");

        var newPath = subject.toggleFeatureFlag("newFlag");

        expect(newPath).toEqual("https://mendix.com?newFlag");
    });

    it("removes a feature flag", () => {
        const subject = new FeatureFlagger("https://mendix.com?oldFlag");

        var newPath = subject.toggleFeatureFlag("oldFlag");

        expect(newPath).toEqual("https://mendix.com");
    });

    it("does not remove unrelated feature flags", () => {
        const subject = new FeatureFlagger("https://mendix.com?oldFlag&desiredFlag");

        var newPath = subject.toggleFeatureFlag("oldFlag");

        expect(newPath).toEqual("https://mendix.com?desiredFlag");
    });

    it("adds a new feature flag at the end", () => {
        const subject = new FeatureFlagger("https://mendix.com?existingFlag&desiredFlag");

        var newPath = subject.toggleFeatureFlag("existingFlag");
        var newPath = subject.toggleFeatureFlag("existingFlag");

        expect(newPath).toEqual("https://mendix.com?desiredFlag&existingFlag");
    });
});
