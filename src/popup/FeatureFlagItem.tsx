import { observer } from "mobx-react";

import * as React from "react";

import { Checkbox } from "@rmwc/checkbox";
import { ListItem, ListItemMeta, ListItemText } from "@rmwc/list";
import { IconButton } from "@rmwc/icon-button";

type Props = {
    featureFlag: string;
    isActive: boolean;

    onClick: (featureFlag: string) => void;
    onDeleteClick: (featureFlag: string) => void;
};

@observer
export class FeatureFlagItem extends React.Component<Props> {
    render() {
        const { featureFlag, isActive } = this.props;

        return (
            <ListItem key={featureFlag} onClick={this.onClick} selected={false}>
                <Checkbox checked={isActive} />
                <ListItemText>{featureFlag}</ListItemText>
                <ListItemMeta onClick={this.onDeleteClick}>
                    <IconButton icon="delete" />
                </ListItemMeta>
            </ListItem>
        );
    }

    onClick = () => {
        this.props.onClick(this.props.featureFlag);
    };

    onDeleteClick = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
        this.props.onDeleteClick(this.props.featureFlag);

        event.stopPropagation();
    };
}
