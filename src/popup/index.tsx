import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Includes all of material-components-web styles from the latest version
import "material-components-web/dist/material-components-web.min.css";

import { FeatureFlagStore } from "../shared/FeatureFlagStore";

import { DropdownMenu } from './DropdownMenu';
import { FeatureFlagger } from "./FeatureFlagger";

chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    const url = tabs[0].url;

    const featureFlagger = new FeatureFlagger(url);
    const store = new FeatureFlagStore();

    store.loadFromStorage().then(() => {
        ReactDOM.render(<DropdownMenu store={store} featureFlagger={featureFlagger} currentTab={tabs[0]} />, document.getElementById("featureFlags"));
    });
});
