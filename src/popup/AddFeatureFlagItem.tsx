import { observer } from "mobx-react";
import * as React from "react";
import { observable } from "mobx";

import { IconButton } from "@rmwc/icon-button";
import { TextField } from "@rmwc/textfield";

import { ListItem, ListItemMeta } from "@rmwc/list";

type Props = {
    onAdd: (featureFlag: string) => void;
};

@observer
export class AddFeatureFlagItem extends React.Component<Props> {
    @observable value: string = "";

    render() {
        return (
            <ListItem>
                <TextField
                    onChange={this.onChange}
                    onKeyDown={this.onKeyDown}
                    label="Create new..."
                    value={this.value}
                />
                <ListItemMeta>
                    <IconButton icon="add" onClick={this.onAddClick} />
                </ListItemMeta>
            </ListItem>
        );
    }

    onAddClick = (_event: React.MouseEvent<HTMLInputElement>) => {
        this.addIfValid(this.value);
    };

    onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.value = event.currentTarget.value;
    };

    onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.keyCode == 13) {
            this.addIfValid(this.value);
        }
    };

    addIfValid(value: string): void {
        if (this.isValid(value)) {
            this.props.onAdd(value);

            this.value = "";
        }
    }

    isValid(value: string): boolean {
        if (value.trim() === "") return false;

        if (value.match(/^[a-zA-Z0-9=_%\-]+$/)) return true;

        return false;
    }
}
