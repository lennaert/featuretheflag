import { observable } from 'mobx';

export class FeatureFlagger {
    @observable activeFeatureFlags: string[] = [];

    constructor(private readonly url: string) {
        this.activeFeatureFlags = this.determineFeatureFlags();
    }

    toggleFeatureFlag(flag: string) {
        const splitUrl = this.splitUrl();
        const path = splitUrl[0];

        const shouldAdd = !this.activeFeatureFlags.includes(flag);

        this.activeFeatureFlags = this.removeFlags([flag]);

        if (shouldAdd) {
            this.activeFeatureFlags.push(flag);
        }

        return path + this.formatFlags();
    }

    determineFeatureFlags() {
        var splitUrl = this.splitUrl();
        var search = splitUrl[1];

        return search.substr(1).split("&");
    }

    splitUrl() {
        var questionMarkIndex = this.url.indexOf("?");

        if (questionMarkIndex == -1) {
            return [this.url, ""];
        } else {
            return [this.url.substring(0, questionMarkIndex), this.url.substring(questionMarkIndex)];
        }
    }

    removeFlags(flags: string[]) {
        return this.activeFeatureFlags.filter(part => !flags.includes(part) && part != "");
    }

    formatFlags() {
        if (this.activeFeatureFlags.length) {
            return "?" + this.activeFeatureFlags.join("&");
        } else {
            return "";
        }
    }
}
