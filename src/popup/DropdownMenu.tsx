import { computed } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";

import { FeatureFlagStore } from "../shared/FeatureFlagStore";

import { FeatureFlagger } from "./FeatureFlagger";
import { FeatureFlagItem } from "./FeatureFlagItem";
import { AddFeatureFlagItem } from "./AddFeatureFlagItem";

import { List, ListItem } from "@rmwc/list";
import { ThemeProvider } from "@rmwc/theme";
import {
    TopAppBar,
    TopAppBarRow,
    TopAppBarSection,
    TopAppBarTitle,
    TopAppBarActionItem,
    TopAppBarFixedAdjust
} from "@rmwc/top-app-bar";
import { Typography } from "@rmwc/typography";

type Props = {
    store: FeatureFlagStore;
    featureFlagger: FeatureFlagger;
    currentTab: any;
};

@observer
export class DropdownMenu extends React.Component<Props> {
    @computed
    get flags(): string[] {
        return this.props.store.featureFlagsForDomain(this.url);
    }

    @computed
    get url(): string {
        return this.props.currentTab.url;
    }

    render() {
        return (
            <ThemeProvider
                options={{
                    primary: "#de5151",
                    secondary: "#5d5dff"
                }}>
                {this.renderTopBar()}
                <List>
                    {this.renderExistingFeatureFlags()}
                    <AddFeatureFlagItem onAdd={this.onFeatureFlagAdded} />
                </List>
            </ThemeProvider>
        );
    }

    renderTopBar() {
        return (
            <>
                <TopAppBar>
                    <TopAppBarRow>
                        <TopAppBarSection alignStart>
                            <TopAppBarTitle>Feature flags</TopAppBarTitle>
                        </TopAppBarSection>
                        <TopAppBarSection alignEnd>
                            <TopAppBarActionItem icon="settings" onClick={this.onSettingsIconClick} />
                        </TopAppBarSection>
                    </TopAppBarRow>
                </TopAppBar>
                <TopAppBarFixedAdjust />
            </>
        );
    }

    renderExistingFeatureFlags() {
        if (this.flags.length > 0) {
            return this.flags.map(featureFlag => {
                const isActive = this.props.featureFlagger.activeFeatureFlags.includes(featureFlag);

                return (
                    <FeatureFlagItem
                        isActive={isActive}
                        featureFlag={featureFlag}
                        onClick={this.featureFlagClicked}
                        onDeleteClick={this.onDeleteFeatureFlagClick}
                    />
                );
            });
        } else {
            return (
                <Typography use="body1">
                    <ListItem disabled={true}>No feature flags configured for this domain</ListItem>
                </Typography>
            );
        }
    }

    featureFlagClicked = (featureFlag: string) => {
        var newUrl = this.props.featureFlagger.toggleFeatureFlag(featureFlag);

        chrome.tabs.update(this.props.currentTab.id, { url: newUrl });
    };

    onFeatureFlagAdded = (featureFlag: string) => {
        this.props.store.add(this.url, featureFlag);
    };

    onDeleteFeatureFlagClick = (featureFlag: string) => {
        this.props.store.delete(this.url, featureFlag);
    };

    onSettingsIconClick = () => {
        chrome.runtime.openOptionsPage();
    };
}
