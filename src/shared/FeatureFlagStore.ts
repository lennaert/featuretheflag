import { observable, IObservableArray, action, computed } from "mobx";

type SiteConfiguration = {
    domain: string;
    featureFlags: IObservableArray<string>;
};

type StoredConfiguration = {
    siteConfigurations: string;
};

const RUNNING_IN_PAGE = true;
// const RUNNING_IN_PAGE = false;

export class FeatureFlagStore {
    @observable loading = false;

    @observable siteConfigurations: IObservableArray<SiteConfiguration> = observable([]);

    async loadFromStorage() {
        const result = await this.getFromStorageBackend("siteConfigurations");

        if (!result) {
            return;
        }

        (this.siteConfigurations as IObservableArray<SiteConfiguration>).replace(
            this.extractStoredValues(result.siteConfigurations)
        );
    }

    async getFromStorageBackend(key: string): Promise<StoredConfiguration> {
        if (RUNNING_IN_PAGE) {
            const item = window.localStorage.getItem(key);

            if (item == "") {
                return { siteConfigurations: "" };
            } else {
                return { siteConfigurations: item };
            }
        }

        return new Promise(resolve => {
            chrome.storage.local.get([key], (result: StoredConfiguration) => {
                resolve(result);
            });
        });
    }

    featureFlagsForDomain(url: string): IObservableArray<string> {
        const tld = this.extractTld(url);

        const siteConfiguration = this.siteConfigurations.filter(conf => conf.domain == tld)[0];

        if (siteConfiguration) {
            return siteConfiguration.featureFlags;
        } else {
            const newSiteConfiguration = this.addDomainWithoutSaving(url);

            return newSiteConfiguration.featureFlags;
        }
    }

    @computed
    get domains(): string[] {
        return this.siteConfigurations.map(conf => conf.domain);
    }

    @action
    deleteDomain(domain: string) {
        // Assume that the given domain is exact: It was stripped when it was added
        const matchingSiteConfiguration = this.siteConfigurations.filter(conf => conf.domain == domain)[0];

        if (!matchingSiteConfiguration) {
            return;
        }

        this.siteConfigurations.remove(matchingSiteConfiguration);

        this.saveToStorage();
    }

    @action addDomainWithoutSaving(url: string): SiteConfiguration {
        const tld = this.extractTld(url);

        const featureFlags: IObservableArray<string> = observable([]);

        const newSiteConfiguration = { domain: tld, featureFlags: featureFlags };

        this.siteConfigurations.push(newSiteConfiguration);

        return newSiteConfiguration;
    }

    @action
    add(domain: string, featureFlag: string) {
        this.featureFlagsForDomain(domain).push(featureFlag);

        this.saveToStorage();
    }

    @action
    delete(domain: string, featureFlag: string) {
        this.featureFlagsForDomain(domain).remove(featureFlag);
        this.saveToStorage();
    }

    private extractStoredValues(siteConfigurationsJson: string | undefined): SiteConfiguration[] {
        if (siteConfigurationsJson == undefined || siteConfigurationsJson == "") {
            return [];
        } else {
            return JSON.parse(siteConfigurationsJson);
        }
    }

    private extractTld(url: string): string {
        const withoutProtocol = url.replace(/^https?:\/\//, "");

        const firstSlashIndex = withoutProtocol.indexOf("/");

        if (firstSlashIndex < 0) {
            return this.stripPort(withoutProtocol);
        }

        const withoutPath = withoutProtocol.substring(0, firstSlashIndex);

        return this.stripPort(withoutPath);
    }

    private stripPort(url: string): string {
        const firstColonIndex = url.indexOf(":");

        if (firstColonIndex < 0) {
            return url;
        }

        return url.substring(0, firstColonIndex);
    }

    private saveToStorage() {
        const json = JSON.stringify(this.siteConfigurations);

        this.setInStorageBackend("siteConfigurations", json);
    }

    setInStorageBackend(key: string, json: string) {
        if (RUNNING_IN_PAGE) {
            window.localStorage.setItem(key, json);
        } else {
            const data = {};
            (data as any)[key] = json;

            chrome.storage.local.set(data);
        }
    }
}
