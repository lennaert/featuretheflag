# Introduction

This extension allows quick addition/removal of terms to a URL.

For example: changing `https://example.com/some_page` to `https://example.com/some_page?enableFoo`.

# Usage

Install the extension. It will add an icon to the extension bar.

Clicking on the icon shows a dropdown where you can see the currently
available items. Click on an item to add it to the URL, click on it again to remove it.

This also works with multiple items.

You can add items by clicking 'Add item'. This does not immediately activate it.

There is also an overview page. Right-click on the icon and click 'Options'. The
options page will open, showing all sites and all items that are configured.

# Building

First get all packages: `yarn install`.

To run a development server (watching and rebuilding after changes): `yarn dev`

To create a production version: `yarn dist`.

# Development

-   Visit chrome://extensions/ in Google Chrome
-   Enable Developer mode in Google Chrome (top right)
-   Click 'Load unpacked' and point to the `dist` directory

The plugin should auto-reload, so you shouldn't need to add/remove
on changes.

# Releasing

A script is included to build for production and create a git tag: `pack-release.rb`.

You need Ruby to run that script.

Now go to https://chrome.google.com/u/1/webstore/devconsole/, log in as lennaertmeijvogel.dev and upload the generated .zip file.
